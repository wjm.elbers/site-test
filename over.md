Aandacht hebben voor wat er écht is, wat er toe doet. Samen stil staan, kijken en een diepe verbinding leggen. Met de rust, respect en het vertrouwen dat dit verdient. Dit staat centraal in de manier waarop José werkt.

José heeft in 2011 de Post HBO opleiding 'Omgaan met Verlies' afgerond bij Land van Rouw en daarna de therapeutische vervolgopleiding 'Werken met Gestolde Rouw'. Bij Phoenixopleidingen volgde zij onder andere de Masterclass opleiding met Wibe Veenbaas en de vervolgopleiding 'Op de rand van het licht, over rouw en ritueel'.

José werkt met verschillende methodieken zoals de presentiebenadering, systemisch werk, opstellingen, lijfwerk en regressiewerk. Samen met haar cliënten kijkt zij welke methodiek het beste aansluit. Ze laat zich in het werkveld inspireren door mensen als Wibe Veenbaas, Bert Hellinger, Franz Ruppert, Sabine Noten, Riet Fiddelaers-Jaspers en Gwen Timmer.

```
"Als het niet kan zoals het moet, 
dan moet het maar zoals het kan"
Wijze woorden van mijn moeder
```

In het leven kom je gebeurtenissen tegen die je op de proef stellen. Wat je tegenkomt en op welke manier je hier mee omgaat weet je van te voren niet. Soms kan het lastig zijn de juiste weg weer te vinden.

Dat het leven naast veel moois ook tegenslagen in petto heeft is mij bekend. Door (verlies)ervaringen, zelfreflectie en kijken naar mijn proces, leer ik mezelf steeds beter kennen. Door mijn eigen toppen en dalen te bekijken, steeds weer opnieuw, kan ik anderen ook ontmoeten in hun proces. Contact gaat voor mij over wie je bent als mens en hoe en waarop je elkaar ontmoet.

Ik laat mij in mijn privéleven o.a. inspireren door mijn man, mijn kinderen en het gemis van mijn kind. Verder zijn ook andere mensen die me dierbaar zijn en die ik ontmoet een inspiratiebron, net als de natuur en bezig zijn met mijn handen.

```
"Het leven wordt voorwaarts geleefd en achterwaarts begrepen"
Kierkegaard
```