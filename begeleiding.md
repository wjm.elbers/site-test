De eigen authenticiteit is het uitgangspunt van de begeleiding, alles wat je tegen bent gekomen op je weg draag je op een bepaalde manier met je mee. We onderzoeken samen wat je nodig hebt om je draagkracht nog verder neer te zetten en balans te gaan voelen. Hierbij is er aandacht voor herstel enerzijds en het verlies anderzijds.

```
“Rouw is de achterkant van de liefde”
```

Waarvoor kan je bij mij terecht?

* Als een situatie in het hier en nu maakt dat je de weg even kwijt bent en handvatten wilt hoe hiermee om te gaan.
* Als je steeds weer vastloopt op je werk of in je prive en wilt weten wat hieraan ten grondslag ligt.
* Je hebt een verlies meegemaakt van een dierbare, je thuis, werk, relatie, gezondheid, dromen of verlangens. Dit kan recent zijn geweest maar ook al lang geleden. Hierbij vind je het lastig om de draad van het leven weer op te pakken.
* Je hebt het gevoel dat je er alleen voor staat en wilt kijken wat je hiermee kan.
* Je hebt je wensen of dromen los moeten laten wat je nog zwaar kan vallen.

Meer specifieke thema’s waarvoor je ook bij mij terecht kunt zijn o.a.:

* Je hebt je zoon of dochter verloren.
* Je krijgt te maken met een euthanasie traject en wilt kijken wat je nodig hebt om hiermee om te gaan.
* Je hebt veel (verlies) meegemaakt in je leven en voelt dat het tijd is om hiernaar te kijken. Je wilt graag handvatten zodat je dit kan vertalen naar de toekomst.
* Het verlies is gekoppeld aan de psychiatrie.

Voor supervisie trajecten ben ik als supervisor verbonden aan Land van Rouw.

Je bent van harte welkom, met alles wat er is.
Ik ontmoet je graag.

Duur van een sessie is 60 tot 75 minuten. 
Kosten op aanvraag. 

Annulering: een afspraak kan tot 24 uur van tevoren worden geannuleerd via email of telefoon. Als de annulering binnen de 24 uur valt wordt 50% van het gevraagde tarief in rekening gebracht (met uitzondering van bijzondere omstandigheden). In het geval van een no-show wordt 100% van het tarief in rekening gebracht.