Naast levenservaringen draagt het jezelf scholen en ontwikkelingen bij aan dat je steeds dichter bij jezelf komt. De verbinding leggen met je roots, hoe je nu in het leven staat en welke weg er voor je ligt kunnen bijdragen in je leven, persoonlijk én professioneel. Dit maakt dat je meer jezelf kunt zijn en draagt bij in hoe je de ander kunt ontmoeten. José Bosman, roots, biedt trainingen aan die hierop aansluiten.

Kosten op aanvraag.

```
"De deur van het geluk gaat naar binnen open"
Kierkegaard
```