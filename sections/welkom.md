Welkom op de site van José Bosman, roots.

Het leven kenmerkt zich door een aaneenschakeling van veranderingen.

Bij José Bosman, roots, is er aandacht voor verandering en verlies. Roots gaat over de basis en welke weg afgelegd is. Daarbij hoort ook wat er nodig is om persoonlijk en professioneel nog verder te wortelen, te groeien en te ontwikkelen. Ieder op zijn of haar eigen tempo. De eigen basis is het uitgangspunt om in het hier en nu op te bouwen.

Het verliezen van iets wat je dierbaar is, zoals een persoon, relatie, werk, gezondheid of een thuis, maakt dat je wereld er in een klap anders uit kan zien. Daarnaast kunnen ogenschijnlijk kleine gebeurtenissen een trigger zijn om oud verlies weer naar boven te laten komen.

Het is aan ieder hoe hij of zij hiermee om gaat en om kan gaan. Het erkennen van je verlies en dit laten doordringen is een intensieve stap.

"Waar je ook gaat, daar ben je"
Jon Kabat-Zinn
Het kan voelen alsof de wereld stil staat of instort, er is zoveel wat er op je af komt, dat het te veel lijkt om aan te gaan. De diversiteit van gevoelens is groot. Het herkennen van deze gevoelens kan helpen er meer grip op te krijgen.

Soms is leven echt overleven.

Het vraagt moed om je eigen weg weer te vinden en om stil te staan bij het verlies. Daarbij hoort ook om op een voor jou gepaste manier richting geven aan hoe je in de toekomst weer kan leven met het verlies dat je in je hart bij je draagt.

Ieders proces is uniek. Soms is het prettig als iemand even met je meeloopt hierin en jou het begrip, de ruimte en erkenning geeft die je nodig hebt, zodat je de draad van het leven weer kunt oppakken. Daarvoor kun je terecht bij José Bosman, roots.